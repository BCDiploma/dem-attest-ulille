## Contexte

Le projet Dem-Attest-ULille démontre qu’un projet blockchain de transformation digitale au sein d’une université est un levier puissant pour :
* Rendre un service immédiat aux étudiants, en leur fournissant des
diplômes et blocs de compétences 100 numériques, multilingues,
infalsifiables, reconnus par tous et dans le monde entier
* Automatiser, fiabiliser et réduire les coûts de production et de
vérification des attestations académiques
* Renforcer la confiance envers les institutions et les documents
qu’elles délivrent, en permettant entre autres aux employeurs de
disposer sans démarche de documents infalsifiables

Vous pouvez trouver le livre blanc Dem-Attest-ULille "Attestations numériques blockchain de réussite au diplôme de l’Université de Lille" à cette adresse : https://www.bcdiploma.com/img/pdf/20220114_Livre_blanc_Dem-Attest-ULille_FR.pdf

## Objet de ce repository  

Ce repository a pour objectif de partager, en opensource, certaines ressources relatives au projet Dem-Attest-ULille, à savoir:

* Enquête de satisfaction auprès des étudiants
* Trame de la CONVENTION DE SOUS TRAITANCE DE TRAITEMENT DE DONNÉES À CARACTÈRE PERSONNEL signée entre l’Université de Lille et BCdiploma
* Requête d’extraction des données APOGEE
* Table des traductions de l’offre de formation (extrait)
